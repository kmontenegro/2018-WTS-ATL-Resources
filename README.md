# Weathering The Storms  
## 2018 Atlanta Resources  
* ILRC family toolkit  
    * [Family Preparedness Kit](https://www.ilrc.org/family-preparedness-plan)  
* Witness filming ICE resources  
    * [Filming Immigration and Custom Officials](https://witness.org/filming-ice/)  
* Glossary (english)  
    * [Access Now Digital Security Glossary](https://www.accessnow.org/first-look-digital-security-glossary/)   
* Doxxing resources  
    * [Equality Labs Doxxing Guide](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)  
    * [OSWN Doxxing Guide](https://www.ohshitwhatnow.org/2017/06/26/resisting-doxing-protecting-privacy-resources-vulnerable/)   
    * [Who Gets Doxxed & How To Detect It](https://boingboing.net/2017/11/14/dropping-dox.html)   